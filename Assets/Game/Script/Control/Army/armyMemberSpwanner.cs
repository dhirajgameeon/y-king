using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class armyMemberSpwanner : MonoBehaviour
{
    WarriorManager armyManager;
    [SerializeField] GameObject objectWaitTimer;
    [SerializeField] float floatWaitTimer;
    [SerializeField] float currrentTimer = 1;
    public Transform spwanPosition;
    Image imageWaitTimer;
    void Start()
    {
        armyManager = WarriorManager.instance;        
        imageWaitTimer = objectWaitTimer.GetComponentInChildren<Image>();
    }

    private void Update()
    {
        spwanArmy();
    }
    void spwanArmy()
    {
        if (currrentTimer <= 0 || armyManager.isArmyOnWar) objectWaitTimer.SetActive(false);
        if(armyManager.ArmyMembers.Count>0 && armyManager.Position.Count > 0 && !armyManager.isArmyOnWar)
        {
            
            if (currrentTimer > 0)
            {
                if (!objectWaitTimer.activeSelf) objectWaitTimer.SetActive(true);
                currrentTimer -= Time.deltaTime;
                imageWaitTimer.fillAmount = currrentTimer / floatWaitTimer;
                objectWaitTimer.transform.forward = -Camera.main.transform.forward;
                if (currrentTimer <= 0)
                {
                    AM.am.aS.PlayOneShot(AM.am.WSpwan);
                    GameObject aMember = armyManager.ArmyMembers[0];
                    armyManager.ActiveArmyMembers.Add(aMember);
                    armyManager.ArmyMembers.Remove(armyManager.ArmyMembers[0]);
                    aMember.transform.position = spwanPosition.position;
                    aMember.SetActive(true);
                    aMember.GetComponent<aMemberMove>().agent.stoppingDistance = 0;
                    aMember.GetComponent<aMemberMove>().stoppingDistance = 0;
                    aMember.GetComponent<aMemberMove>().followingPlayer = false;
                    aMember.GetComponent<playerHealthControl>().health.HP = aMember.GetComponent<playerHealthControl>().health.MaxHealth;
                    aMember.GetComponent<aMemberMove>().DestinationToMove = armyManager.Position[0].transform;
                    armyManager.Position[0].GetComponent<waitingArea>().isOccupied = true;
                    aMember.transform.position = spwanPosition.position;
                    currrentTimer = floatWaitTimer;
                }
            }
        }
        else
        {
            return;
        }
    }


}
