using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waitingArea : MonoBehaviour
{
    WarriorManager wManager;
    public bool isOccupied = false;
    public float rotatingFace = 90;
    void Start()
    {
        wManager = WarriorManager.instance;
    }
    
    void Update()
    {
        addPositionToBucketList();
    }
    void addPositionToBucketList()
    {
        if (!isOccupied && !wManager.Position.Contains(this.gameObject)) wManager.Position.Add(this.gameObject);
        if (isOccupied && wManager.Position.Contains(this.gameObject)) wManager.Position.Remove(this.gameObject);
    }
}
