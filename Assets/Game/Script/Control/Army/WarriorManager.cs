using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorManager : MonoBehaviour
{
    public static WarriorManager instance;
    public List<GameObject> Position = new List<GameObject>();
    public List<GameObject> ArmyMembers = new List<GameObject>();
    public List<GameObject> ActiveArmyMembers = new List<GameObject>();

    [SerializeField] Transform Player;
    public bool isArmyOnWar = false;

    private movePlayer movePlayer;
    private playerControl playerController;
    public void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        movePlayer = Player.GetComponent<movePlayer>();
        playerController = Player.GetComponent<playerControl>();
        
    }

    private void Update()
    {
        foreach(GameObject obj in ArmyMembers)
        {
            if(obj.activeSelf)
                obj.SetActive(false);
        }
        onClickBoard();
        aMTPL();
    }
    bool amwp;
    void onClickBoard(){
        if (amwp && isArmyOnWar && playerController.aManWithPlayer.Count <= 0)
        {
            isArmyOnWar = false;
            amwp = false;
        }
        if(playerController.aManWithPlayer.Count > 0)
            amwp = true;

        /*  if(playerController.isPlayerOnBoat){
              foreach(GameObject a in ActiveArmyMembers){
                  if(a.GetComponent<aMemberMove>().enabled){
                      a.GetComponent<aMemberMove>().enabled=false;
                      a.GetComponent<aMemberMove>().agent.isStopped=true;
                  }
              }
          }*/
    }


    bool followPlayerStart;

    public void SetPlayerPosition()
    {
        followPlayerStart = true;
        FindObjectOfType<TutorialManager>().calledOnce = true;
        FindObjectOfType<TutorialManager>().win = false;
    }

    //add memebers to player's list
    public void aMTPL()
    {
        if (ActiveArmyMembers.Count > 0 && followPlayerStart)
        {
            foreach (GameObject item in ActiveArmyMembers.ToArray())
            {
                aMemberMove itemMove = item.GetComponent<aMemberMove>();
                if (!playerControl.instance.aMan.Contains(itemMove.gameObject))
                {
                    playerControl.instance.aMan.Add(itemMove.gameObject);
                    ActiveArmyMembers.Remove(itemMove.gameObject);
                    itemMove.enabled = true;
                    itemMove.followingPlayer = true;
                    itemMove.DestinationToMove.GetComponent<waitingArea>().isOccupied = false;
                    itemMove.DestinationToMove = Player;
                    itemMove.agent.isStopped = false;
                    itemMove.agent.stoppingDistance = movePlayer.maintainDistanceFromArmy;
                    itemMove.stoppingDistance = movePlayer.maintainDistanceFromArmy;
                }
            }           
            /* if(followPlayerStart && isArmyOnWar && ActiveArmyMembers.Count>0 && !playerControl.instance.isPlayerOnBoat)
             {             
                 if (ActiveArmyMembers.Count <= 0) followPlayerStart = false;
             }  */

        }
        if (followPlayerStart && ActiveArmyMembers.Count <= 0 && !playerControl.instance.isPlayerOnBoat)
        {
            /*item.GetComponent<aMemberMove>().DestinationToMove = Player;
            item.GetComponent<aMemberMove>().agent.stoppingDistance = movePlayer.maintainDistanceFromArmy;*/
            isArmyOnWar = true;
            followPlayerStart = false;
        }
    }
}
