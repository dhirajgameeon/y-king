using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour
{
    public bool isPlayerOnPlate = false;
    public GameObject FollowButton;
    

    WarriorManager armyManager;
    private void Start()
    {
        armyManager = WarriorManager.instance;
    }
    private void Update()
    {
        ActivateFollowButton();
    }

    void ActivateFollowButton()
    {
        if (armyManager.ActiveArmyMembers.Count > 0 || !armyManager.isArmyOnWar)
        {
            if (isPlayerOnPlate && !FollowButton.activeSelf) FollowButton.SetActive(true);            
        }
        if (!isPlayerOnPlate && FollowButton.activeSelf) FollowButton.SetActive(false);
        if (armyManager.isArmyOnWar && FollowButton.activeSelf && armyManager.ActiveArmyMembers.Count <= 0) FollowButton.SetActive(false);

    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude <= 0.1f)
        {
            isPlayerOnPlate = true;     
        }
        if(other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude > 0.1f)
        {
            isPlayerOnPlate = false;     
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerOnPlate) isPlayerOnPlate = false;
    }
}
