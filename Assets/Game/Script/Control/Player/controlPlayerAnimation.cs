using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPlayerAnimation : MonoBehaviour
{
    public Animator Animation;

    private movePlayer movePlayer;

    void Start()
    {
        movePlayer = GetComponent<movePlayer>();

    }


    void Update()
    {
        characterAnimation();
    }

    void characterAnimation()
    {
        Animation.SetFloat("speed", movePlayer.direction.magnitude);
    }
}
