using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class playerControl : MonoBehaviour
{
    public static playerControl instance;
    public List<GameObject> aMan = new List<GameObject>();
    public List<GameObject> aManWithPlayer = new List<GameObject>();
    public bool isPlayerOnBoat;
    public bool isPlayerLeftBoat;
    public Transform endPosition;
    public GameObject Boat;
    public float boostedAttackDamage;
    public float boostingArea;
    public Animator Cam;

    private WarriorManager wManager;
    
    playerHealthControl Health;

    public bool isPlayerOnDeck = false;

    // Start is called before the first frame update

    private void Awake() {
        instance=this;
    }
    private void Start()
    {
        wManager = WarriorManager.instance;
        Boat = GameObject.Find("Boat");
        Health = GetComponent<playerHealthControl>();
    }

    // Update is called once per frame
    void Update()
    {
        cIsPOB();
        GameOver();
        if (isPlayerOnBoat) Cam.Play("Boat");
        if (!isPlayerOnBoat) Cam.Play("Player");
    }

    public void GameOver()
    {
        if (Health.health.HP <= 0)
        {
            GameManager.instance.GameOver = true;
        }
    }

    //Check is player on boat
    public void cIsPOB()
    {
        if(isPlayerOnBoat && aMan.Count > 0 && Boat.GetComponent<BoatControl>().BoatSeattingPosition.Count <= 0)
        {
            foreach (GameObject aM in aMan.ToArray()) {
                
                wManager.ActiveArmyMembers.Add(aM);
                aM.GetComponent<aMemberMove>().DestinationToMove = wManager.Position[0].transform;
                aM.GetComponent<aMemberMove>().DestinationToMove.GetComponent<waitingArea>().isOccupied = true;
                aM.GetComponent<aMemberMove>().stoppingDistance = 0;
                aM.GetComponent<aMemberMove>().agent.stoppingDistance = 0;
                aM.GetComponent<aMemberMove>().followingPlayer = false;
                aMan.Remove(aM);                   
                /* if (!ArmyManager.instance.ActiveArmyMembers.Contains(aM)) ;
                 {
                     print("Adding to army list");
                 }*/
            }
            
        }
        if (aMan.Count > 0)
        {
            foreach (GameObject aM in aMan)
            {
                if(!aManWithPlayer.Contains(aM))
                    aManWithPlayer.Add(aM);
            }
        }
        if (aManWithPlayer.Count > 0)
        {
            foreach (GameObject aM in aManWithPlayer.ToArray())
            {
                if (!aM.GetComponent<aMemberMove>().followingPlayer)
                    aManWithPlayer.Remove(aM);
            }
        }
        if(aManWithPlayer.Count > 0)
        {
            for (int i = 0; i < aManWithPlayer.Count; i++)
            {
                if (!aManWithPlayer[i].activeSelf)
                    aManWithPlayer.Remove(aManWithPlayer[i]);
            }
        }
        if (aMan.Count > 0)
        {
            for (int i = 0; i < aMan.Count; i++)
            {
                if (!aMan[i].activeSelf)
                    aMan.Remove(aMan[i]);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coin"))
        {            
            if (other.gameObject.activeSelf)
            {
                AM.am.aS.PlayOneShot(AM.am.CC);
                GameManager.instance.maxCoin +=
                    other.gameObject.GetComponent<coin>().coinValue;
                //hapticFeedbackManager.Vibrate(GameManager.instance.vibrationLenght);
                HapticFeedback.Vibrate(UIFeedbackType.ImpactHeavy);
                other.gameObject.GetComponent<coin>().PE();
                LevelManager.Instance.saveCoin();
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.GetComponent<deboardDeck>())
        {
            isPlayerOnDeck = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(isPlayerOnDeck)
            isPlayerOnDeck=false;
    }


    /*
      foreach (GameObject aM in aMan) {
               print("Adding to army list");
               ArmyManager.instance.ActiveArmyMembers.Add(aM);
               aMan.Remove(aM);
               //ArmyManager.instance.ActiveArmyMembers[ArmyManager.instance.ActiveArmyMembers.Count - 1].GetComponent<aMemberMove>().DestinationToMove = ArmyManager.instance.Position[ArmyManager.instance.Position.Count-1].transform;
               //ArmyManager.instance.ActiveArmyMembers[ArmyManager.instance.ActiveArmyMembers.Count - 1].GetComponent<aMemberMove>().DestinationToMove.GetComponent<waitingArea>().isOccupied = true;                
               print("Added to army list");
           }

    */
}
