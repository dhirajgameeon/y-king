using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAutoAttack : MonoBehaviour
{
    public GameObject enemyToAttack;
    public Animator animator;
    [Space(20)]
    public Collider[] NearByObject;
    public LayerMask Enemy;
    public bool isAttacking = false;
    private movePlayer move;
    private playerControl pControl;
    private playerHealthControl PHC;

    float x = 0;
    private void Start()
    {
        pControl = playerControl.instance;
        move = GetComponent<movePlayer>();
        PHC = GetComponent<playerHealthControl>();
        x= PHC.health.attackSpeed;
    }
    private void Update()
    {
        //checkForNearByEnemy();
    }
    public void checkForNearByEnemy( Collider other)
    {
        if(!pControl.isPlayerOnBoat && move.direction.magnitude< 0.1f)
        {
            //NearByObject = Physics.OverlapSphere(transform.position, PHC.warrior.AttackRange, Enemy);
            if (/*NearByObject.Length > 0*/!enemyToAttack)
            {
                enemyToAttack = other.gameObject;/*NearByObject[0].gameObject;*/
                if (enemyToAttack && !enemyToAttack.GetComponent<eMemberControlAi>().pWarrior) enemyToAttack.GetComponent<eMemberControlAi>().pWarrior = this.gameObject;
                isAttacking = true;
            }
            if(enemyToAttack && enemyToAttack.GetComponent<eMemberHealthControl>().health.HP > 0)
                Attack();
        }
       /* if (*//*NearByObject.Length <= 0 || move.direction.magnitude > 0.1f*//*)
        {
            isAttacking = false;
            enemyToAttack = null;
        }*/
    }
    
    public void Attack()
    {
        if (enemyToAttack && isAttacking && !animator.GetCurrentAnimatorStateInfo(0).IsName("Standing Melee Attack Downward"))
        {
            if (x < PHC.health.attackSpeed)
                x += Time.deltaTime;
            if (x >= PHC.health.attackSpeed)
            {
                animator.SetTrigger("attack");
                x = 0;
            }
        }
    }
    public void hitEnemy()
    {
        if (enemyToAttack)
        {
            enemyToAttack.GetComponent<eMemberHealthControl>().health.HP -= PHC.health.attackDamage;
        }
    }
    public void hitBlood()
    {
        if (enemyToAttack)
        {
            enemyToAttack.GetComponent<eMemberHealthControl>().health.blood.Play();
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("eCol"))
        {
            checkForNearByEnemy(other);
            //print("<color=green>True : </color>" + other.transform.name);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (enemyToAttack)
        {
            enemyToAttack = null;
        }
        }
    }
