using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class playerHealthControl : MonoBehaviour
{
    public int boostArea = 10;
    public Image healthIndicator;
    public HealthAttribute health;
    public PlayerWarrior warrior;
    
    void Start()
    {
        
    }
    float a;
    void Update()
    {
        if (healthIndicator)
        {
            a = Mathf.Abs((health.HP / health.MaxHealth) - 1);
            if(a>=0.5f)
                healthIndicator.color = new Color(healthIndicator.color.r, healthIndicator.color.g, healthIndicator.color.b, a);
            if (a < 0.5f)
            {
                healthIndicator.color = new Color(healthIndicator.color.r, healthIndicator.color.g, healthIndicator.color.b, 0);
            }
        }
        
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = warrior.AttackRangeColor;
        Handles.DrawWireDisc(transform.position, warrior.discDirection, warrior.AttackRange, 7);
        Handles.color = Color.yellow;
        Handles.DrawWireDisc(transform.position, warrior.discDirection, boostArea, 5);
    }
#endif
}
