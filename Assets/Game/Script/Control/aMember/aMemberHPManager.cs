using UnityEngine;

public class aMemberHPManager : MonoBehaviour

{
    public aMemberControl HP;

    private void Start()
    {
    }
    public void reduceHP()
    {
        HP.hitEnemy();
        
    }

    public void vibrate()
    {
        HapticFeedback.Vibrate(UIFeedbackType.ImpactHeavy);
        //hapticFeedbackManager.Vibrate(GameManager.instance.vibrationLenght);
    }
    public void hitBlood()
    {
        HP.hitBlood();
    }
}
