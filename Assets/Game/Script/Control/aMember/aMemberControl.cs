using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class aMemberControl : MonoBehaviour
{
    /// <summary>
    /// bug list
    /// 1. Enemy is getting loked in attack state, so after respawanning it start playing attack animation
    /// </summary>
    public aMemberMove move;
    public playerHealthControl hCon;
    public Animator animator;
    public Transform parant;
    public ParticleSystem deathEffect;

    public float currentAttackDamage;


    public float deathDealyTime = 0.5f;

    public bool isDead = false;

    private bool isOnAttack;
    public TownHallManager hallManager;
    float x;
    private void Start()
    {
        x = hCon.health.attackSpeed;
        parant = GameObject.Find("Warriors").transform;
    }
/*    private void OnEnable()
    {
        foreach (materialFadeOut item in MFO.ToArray())
        {
            item.startFadingOut = false;
        }
    }*/
    public void Update()
    {
        cE();
        Dead();
        if (WarriorManager.instance.isArmyOnWar || playerControl.instance.aMan.Contains(this.gameObject)) ;
        GameOver();

        if (!hCon.warrior.isEnemyInAttackRange && isOnAttack && !isDead)
        {
            //animator.SetInteger("attack", 0);
            animator.Play("Unarmed Idle");
            isOnAttack = false;
        }
        if (isOnAttack && Vector3.Distance(transform.position, playerControl.instance.transform.position) <= playerControl.instance.boostingArea)
        {
            hCon.health.attackDamage = playerControl.instance.boostedAttackDamage;
        }
        else
        {
            hCon.health.attackDamage = currentAttackDamage;
        }
    }

    /*public bool isUI;*/
    void cE()
    {
        /*if (isUI) GameManager.instance.report.text = move.DestinationToMove.GetComponent<eMemberControlAi>() ? "True" : "False";*/
        animator.SetBool("isAttacking", hCon.warrior.isEnemyInAttackRange);
        if (move.DestinationToMove && move.DestinationToMove.GetComponent<eMemberControlAi>())
        {
            hCon.warrior.isEnemyInAttackRange = Vector3.Distance(transform.position, move.DestinationToMove.position) <= hCon.warrior.AttackRange ? true : false;
            if (hCon.warrior.isEnemyInAttackRange) isOnAttack = true;
        }
        else
        {
            hCon.warrior.isEnemyInAttackRange = false;
        }
        if (hCon.warrior.isEnemyInAttackRange)
        {
            Attack();
        }
        if (!move.DestinationToMove) hCon.warrior.isEnemyInAttackRange = false;
        if (move.DestinationToMove && move.DestinationToMove.GetComponent<eMemberControlAi>() != null)
        {
            EnemyDead();
        }
        if (move.DestinationToMove && move.DestinationToMove.GetComponent<eMemberHealthControl>() && move.DestinationToMove.GetComponent<eMemberHealthControl>().health.HP <= 0.1f)
            move.DestinationToMove = null;
    }
    void Attack()
    {
        if (x < hCon.health.attackSpeed)
            x += Time.deltaTime;
        if (x >= hCon.health.attackSpeed && !animator.GetCurrentAnimatorStateInfo(0).IsName("Standing Melee Attack Downward"))
        {
            //animator.SetInteger("attack", Random.Range(1, 4));
            AM.am.aS.PlayOneShot(AM.am.WAttack[Random.Range(0, AM.am.WAttack.Length)]);
            animator.SetTrigger("attack");
            x = 0;
        }
    }

    public void hitEnemy()
    {
        if (move.DestinationToMove && hCon.warrior.isEnemyInAttackRange && move.DestinationToMove.GetComponent<eMemberHealthControl>().health.HP > 0)
        {
            move.DestinationToMove.GetComponent<eMemberHealthControl>().health.HP -= hCon.health.attackDamage;
            move.DestinationToMove.GetComponent<eMemberHealthControl>().health.blood.Play();
        }
    }
    public void hitBlood()
    {
        if (move.DestinationToMove && hCon.warrior.isEnemyInAttackRange && move.DestinationToMove.GetComponent<eMemberHealthControl>().health.HP > 0)
        {
            move.DestinationToMove.GetComponent<eMemberHealthControl>().health.blood.Play();
        }
    }
    void EnemyDead()
    {
        if (move.DestinationToMove.GetComponent<eMemberHealthControl>().health.HP <= 0.01f && !playerControl.instance.aMan.Contains(this.gameObject))
        {
            if (hCon.health.HP > 0)
            {
                playerControl.instance.aMan.Add(this.gameObject);
                if (!move.DestinationToMove.gameObject.activeSelf)
                    move.DestinationToMove = null;
            }
        }
    }
    void GameOver()
    {
        if (hallManager && hallManager.cEnemyUnitAlive.Count <= 0)
        {            
            sendWToBase();
        }
    }
    void replaceWafk()
    {
        if (!hallManager.awm.afkWarrior[0].GetComponent<moveAFKW>().isOccupied)
        {
            GameObject afk = hallManager.awm.afkWarrior[0];
            afk.GetComponent<moveAFKW>().isOccupied = true;
            hallManager.awm.afkWarrior.Remove(hallManager.awm.afkWarrior[0]);
            afk.transform.position = transform.position;
            afk.transform.eulerAngles = transform.eulerAngles;
            afk.SetActive(true);
        }
        
    }
    void sendWToBase()
    {

        if (!WarriorManager.instance.ArmyMembers.Contains(this.gameObject))
        {
            replaceWafk();
            WarriorManager.instance.ArmyMembers.Insert(0, this.gameObject);
            playerControl.instance.aManWithPlayer.Remove(this.gameObject);
            transform.parent = parant;
            move.DestinationToMove = null;
            hallManager = null;
            GetComponent<aMemberControl>().enabled = true;
            this.gameObject.SetActive(false);
        }
    }

    void Dead()
    {
        if(hCon.health.HP <= 0)
        {
            isDead = true;
            StartCoroutine(deathDelay(deathDealyTime));
        }
    }

    IEnumerator deathDelay(float t)
    {
        //animator.SetInteger("dead", Random.Range(1, 3));
        GetComponent<aMemberControl>().enabled = false;
        deadAnimation();
        move.DestinationToMove = null;
        hallManager = null;
        yield return new WaitForSeconds(1f);
        deathEffect.Play();
        yield return new WaitForSeconds(t);
        callOnce();
    }
    void callOnce() {
       
        if (!WarriorManager.instance.ArmyMembers.Contains(this.gameObject))
        {
            AM.am.aS.PlayOneShot(AM.am.WDeath[Random.Range(0, AM.am.WDeath.Length)]);
            WarriorManager.instance.ArmyMembers.Insert(0,this.gameObject);
            playerControl.instance.aManWithPlayer.Remove(this.gameObject);
            transform.parent = parant;
/*            move.DestinationToMove = null;
            hallManager = null;*/
            GetComponent<aMemberControl>().enabled = true;
            this.gameObject.SetActive(false);
            isDead = false;
        }        
    }
    void deadAnimation()
    {
        int i = Random.Range(0, 2);
        switch (i)
        {
            case 0:
                animator.Play("Falling Back Death");
                animator.Play("FadeOut");
                break;
            case 1:
                animator.Play("Falling Back Death");
                animator.Play("FadeOut");
                break;
        }
    }
}
