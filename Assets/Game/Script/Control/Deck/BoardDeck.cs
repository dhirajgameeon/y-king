using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class BoardDeck : MonoBehaviour
{
    public bool isBoatAtPort;
    public bool isPlayerOnPort;

    public GameObject UI;

    private BoatControl Boat;    
    
    public playerControl pControl;
    private Joystick joystick;
    private Collider col;
    private deboardDeck DD;
    private void Start() {
        pControl = playerControl.instance;
        joystick = FindObjectOfType<Joystick>();
        col = GetComponent<Collider>();
        DD = GetComponent<deboardDeck>();
    }
    private void Update()
    {
        BoatUIManager();
        boatFunction();
        cifBoatMemberReched();

    }

    void cifBoatMemberReched()
    {
        
        if(Boat && pControl && Boat.aMan.Count == pControl.aManWithPlayer.Count )
        {
            bool bMR = Boat.aMan.TrueForAll(aMan => aMan.GetComponent<aMemberMoveOnBoat>().isReachedDestination == true);
            if (bMR)
                joystick.enabled = true;        
            else joystick.enabled = false;
        }
        else if(pControl && pControl.isPlayerOnBoat && pControl.aManWithPlayer.Count <= 0)
        {
            joystick.enabled = true;
        }
        
    }
    void CollidManager()
    {
        if(pControl.isPlayerOnBoat) col.enabled = false;
        if(!pControl.isPlayerOnBoat) col.enabled = true;
    }

    bool UITrue;
    void BoatUIManager()
    {

        if (isPlayerOnPort && !pControl.isPlayerOnBoat && !isClicked && isBoatAtPort && !UITrue)
        {
            delay();
        }

        if(!isPlayerOnPort || pControl.isPlayerOnBoat)UI.SetActive(false);              
    }
    void delay()
    {
        UI.SetActive(true);
    }

    public bool isClicked=false;

    public void boardOnBoat(){
        UI.SetActive(false);
        isClicked =true;
        joystick.enabled = false;
        pControl.GetComponent<movePlayerAi>().enabled=true;        
        pControl.Boat = Boat.gameObject;
        pControl.endPosition = Boat.GetComponent<BoatControl>().DriverPosition;
        pControl.GetComponent<movePlayerAi>().isDeboard=false;
        pControl.GetComponent<movePlayerAi>().isReched=false;
        
    }
    float x = 0.2f;
    void boatFunction(){

        if(isClicked && Boat && Boat.BoatSeattingPosition.Count > 0 && pControl.aMan.Count > 0){
            if(x>0)x-=Time.deltaTime;
            if(x<=0){
                //add army members to boat list
                if (!Boat.aMan.Contains(pControl.aMan[pControl.aMan.Count - 1]))
                    Boat.aMan.Add(pControl.aMan[pControl.aMan.Count - 1]);
                pControl.aMan.Remove(pControl.aMan[pControl.aMan.Count - 1]);

                //apply modification on boat passangers
                Boat.aMan[Boat.aMan.Count-1].GetComponent<aMemberMove>().agent.isStopped=true;                
                Boat.aMan[Boat.aMan.Count-1].GetComponent<aMemberMove>().agent.enabled=false;                
                Boat.aMan[Boat.aMan.Count-1].GetComponent<aMemberMove>().enabled=false;
                Boat.aMan[Boat.aMan.Count-1].GetComponent<aMemberMoveOnBoat>().enabled=true;
                Boat.aMan[Boat.aMan.Count-1].transform.parent = Boat.transform;
                Boat.aMan[Boat.aMan.Count - 1].GetComponent<aMemberMoveOnBoat>().endPosition = Boat.BoatSeattingPosition[Boat.BoatSeattingPosition.Count - 1];                
                Boat.BoatSeattingPosition[Boat.BoatSeattingPosition.Count - 1].GetComponent<BoatSeat>().isOccupied=true;                
                
                //move memebers to boat
                Boat.aMan[Boat.aMan.Count-1].GetComponent<aMemberMoveOnBoat>().moveToBoat();                 
                x=0.2f;
            }                      
        }
        if(isClicked && pControl.isPlayerOnBoat && pControl.aMan.Count<=0){        
            isClicked=false;
            /*DD.enabled = true;*/
        }
    }




    // void boatFunction(){
    //     if(isClicked && Boat && Boat.BoatSeattingPosition.Count > 0){

    //         for (int i = 0; i < Boat.BoatSeattingPosition.Count;i++)
    //         {
    //             armyManager.ActiveArmyMembers[i].GetComponent<aMemberMove>().agent.isStopped=true;
    //             armyManager.ActiveArmyMembers[i].GetComponent<aMemberMove>().enabled=false;
    //             armyManager.ActiveArmyMembers[i].GetComponent<aMemberMoveOnBoat>().enabled=true;
    //             armyManager.ActiveArmyMembers[i].transform.parent = Boat.transform;
    //             armyManager.ActiveArmyMembers[i].GetComponent<aMemberMoveOnBoat>().endPosition = Boat.BoatSeattingPosition[Boat.BoatSeattingPosition.Count-1];                
    //             Boat.BoatSeattingPosition[Boat.BoatSeattingPosition.Count-1].GetComponent<BoatSeat>().isOccupied=true;                
    //             if(!Boat.aMan.Contains(armyManager.ActiveArmyMembers[i]))Boat.aMan.Add(armyManager.ActiveArmyMembers[i]);     
    //             armyManager.ActiveArmyMembers[i].GetComponent<aMemberMoveOnBoat>().moveToBoat();              
    //         }                    
    //     }
    //     if(Boat && Boat.BoatSeattingPosition.Count - 1 <= 0 && isClicked){
    //         Boat.GetComponent<boatMove>().enabled=true;
    //         isClicked=false;    
    //         }
    // }
    /*
    for (int i = 0; i < aMan.Count-1; )
            {
                GameObject a = aMan[i];
                a.GetComponent<aMemberMove>().enabled=false;
                a.GetComponent<aMemberMoveOnBoat>().endPosition = BoatSeattingPosition[0];
                BoatSeattingPosition[0].GetComponent<BoatSeat>().isOccupied=true;
                a.GetComponent<aMemberMoveOnBoat>().moveToBoat();
                i++;
            }

    */

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude <= 0.1f)
        {
            isPlayerOnPort = true;
            pControl = other.gameObject.GetComponent<playerControl>();
        }
        if (other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude > 0.1f)
        {
            isPlayerOnPort = false;
            pControl = null;
        }
        if (other.gameObject.CompareTag("Boat") && !isBoatAtPort)
        {
            isBoatAtPort = true;
            Boat = other.GetComponent<BoatControl>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerOnPort) isPlayerOnPort = false;
        if (isBoatAtPort) {
            isBoatAtPort = false;
            Boat = null;
            }
    }

}
