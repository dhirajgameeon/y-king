using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deboardDeck : MonoBehaviour
{
    public Transform AssemblyArea;
    public GameObject UI;
    public bool isClicked;
    private GameObject Boat;
    private GameObject Player;
    private playerControl pControl;
    private Collider col;
    private BoardDeck BD;
    void Start()
    {
        pControl = playerControl.instance;
        Player = GameObject.Find("Player");
        col = GetComponent<Collider>();
        BD = GetComponent<BoardDeck>();
    }


    void Update()
    {
        checkBoat();
        DeboardBoat();
    }
    bool UITrue;
    void checkBoat() {
        if (Boat && !pControl.isPlayerLeftBoat && pControl.isPlayerOnBoat && !UITrue) {
            Invoke("delay",0.5f);
            UITrue = true;


        }
        if (!pControl.isPlayerOnBoat || !Boat) {
            UI.SetActive(false);
        }
        if (Boat && Boat.GetComponent<BoatMove>().direction.magnitude > 0.1f) UI.SetActive(false);
    }

    void delay()
    {
        if(Boat && !isClicked)
            UI.SetActive(true);
    }
    public void DeboardBoatButton(){
        UI.SetActive(false);
        
        isClicked = true;
        Boat.GetComponent<BoatMove>().enabled = false;
        pControl.isPlayerLeftBoat = true;
        Player.transform.parent = null;
        pControl.endPosition = AssemblyArea;
        //pControl.isPlayerOnBoat = false;
        Player.GetComponent<movePlayerAi>().isReched = false;
        Player.GetComponent<movePlayerAi>().isDeboard = true;
        Player.transform.parent = null;
        Player.GetComponent<movePlayerAi>().enabled = true;
    }
    float x=0.2f;
    private BoatControl boatControl;
    void DeboardBoat(){
        if(isClicked){
            if(x>0)
            x-=Time.deltaTime;
            if(x<=0 && boatControl.aMan.Count > 0 ){                                
                pControl.aMan.Add(boatControl.aMan[boatControl.aMan.Count-1]);
                boatControl.aMan.Remove(boatControl.aMan[boatControl.aMan.Count-1]);

                pControl.aMan[pControl.aMan.Count-1].transform.parent=null;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMoveOnBoat>().enabled=false;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().enabled=true;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().agent.enabled = true;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().agent.isStopped=false;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().DestinationToMove=Player.transform;
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().agent.stoppingDistance = Player.GetComponent<movePlayer>().maintainDistanceFromArmy;                
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMove>().stoppingDistance = Player.GetComponent<movePlayer>().maintainDistanceFromArmy;  
                pControl.aMan[pControl.aMan.Count-1].GetComponent<aMemberMoveOnBoat>().endPosition.GetComponent<BoatSeat>().isOccupied=false;
                
                x=0.2f;                              
            }
            
        }
        if(isClicked && Boat && boatControl.aMan.Count<=0 && !pControl.isPlayerOnBoat){
            isClicked=false;
            UITrue = false;
            /*BD.enabled = true;*/
        }

    }

    
    private void OnTriggerStay(Collider other) {
        if(other.gameObject.CompareTag("Boat") && other.GetComponent<BoatMove>().direction.magnitude<0.1f)
        {
            Boat=other.gameObject;  
            boatControl=other.GetComponent<BoatControl>();
        }
        if (other.gameObject.CompareTag("Boat") && other.GetComponent<BoatMove>().direction.magnitude > 0.1f)
        {
            Boat = null;
            boatControl = null;
            UITrue = false;
        }
    }
    private void OnTriggerExit(Collider other) {
        if (Boat)
        {
            Boat = null;
            UITrue = false;
        }

        }
    }
