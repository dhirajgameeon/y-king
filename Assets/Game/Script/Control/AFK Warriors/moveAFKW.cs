using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class moveAFKW : MonoBehaviour
{
    private NavMeshAgent agent;
    public afkWManager awm;
    public Transform destinationPosition;
    public Animator anime;

    public bool isOccupied;
    public bool isReched;
    public bool isMoving;


    void Start()
    {
        agent= GetComponent<NavMeshAgent>();
      
    }

    private void OnEnable()
    {
        if (!awm.afkPosition[0].GetComponent<afkPositionControl>().isOccupied)
        {
            Transform afkPosition = awm.afkPosition[0];
            awm.afkPosition.Remove(awm.afkPosition[0]);
            afkPosition.GetComponent<afkPositionControl>().isOccupied = true;
            destinationPosition = afkPosition;
        }
    }
    // Update is called once per frame
    void Update()
    {
        move();
        startdoingSomthing();
    }
    void move()
    {
        if (isMoving) anime.SetFloat("speed", agent.velocity.magnitude);
        anime.SetBool("isreached", isReched);
        if (destinationPosition)
        {
            agent.SetDestination(destinationPosition.position);
            if (Vector3.Distance(transform.position, destinationPosition.position) <= (agent.stoppingDistance + 1) && !isMoving && !isReched)
            {
                isReched = true;                
                LeanTween.rotate(this.gameObject, new Vector3(0, destinationPosition.GetComponent<afkPositionControl>().rotatingFace, 0), 0.3f);
            }            
        }
        if (agent.velocity.magnitude > 0.1f) isMoving = true; else isMoving = false;
    }
    void startdoingSomthing()
    {
        if (isReched)
        {
            anime.SetInteger("hangout", Random.Range(1, 3));
        }
    }

}
