using UnityEngine;

public class afkPositionControl : MonoBehaviour
{
    public afkWManager afkWManager;
    public bool isOccupied = false;
    public float rotatingFace = 90;
    void Start()
    {
        rotatingFace = Random.Range(-360, 360);
    }

    void Update()
    {
        addPositionToBucketList();
    }
    void addPositionToBucketList()
    {
        if (!isOccupied && !afkWManager.afkPosition.Contains(this.transform)) afkWManager.afkPosition.Add(this.transform);
        if (isOccupied /*&& afkWManager.afkPosition.Contains(this.transform)*/) afkWManager.afkPosition.Remove(this.transform);
    }
}
