using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin : MonoBehaviour
{
    public int coinValue = 1;
    public Vector3 position;
    public float force = 1;

    public GameObject particalEffect;
    public GameObject GFX;

    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    private void OnEnable()
    {
        transform.parent = null;
        rb.AddForce(new Vector3(Random.Range(-1f * force, 1f * force), 
            Random.Range(1f , 1f * force) * 3, 
            Random.Range(-1f * force, 1f * force)), 
            ForceMode.Impulse);

    }

    public void PE()
    {
        GetComponent<Collider>().enabled = false;
        particalEffect.SetActive(true);
        GFX.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            rb.isKinematic = true;
            rb.useGravity = false;
            GetComponent<Collider>().isTrigger = true;
        }
    }
}
