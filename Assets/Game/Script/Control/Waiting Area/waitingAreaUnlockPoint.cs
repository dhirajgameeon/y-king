using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class waitingAreaUnlockPoint : MonoBehaviour
{
    public bool isPlayerNear;
    public bool isLocked=true;

    public GameObject Locked;
    public GameObject Unlocked;
    public TextMeshProUGUI coinText;

    public float coinNeedToUnlock;
    public float coinSpeed;
    void Start()
    {
        
    }

    
    void Update()
    {
        CoinManage();
    }

    void CoinManage()
    {
        coinText.text = coinNeedToUnlock.ToString("N0");
        if(isLocked && coinNeedToUnlock <= 0.1f)
        {
            Locked.SetActive(false);
            if (!Unlocked.activeSelf)
            {
                AM.am.aS.PlayOneShot(AM.am.UnlockOrUpgrade);
                SaveAndLoadData.instance.saveData();
                LevelManager.Instance.saveCoin();
                Unlocked.SetActive(true);
            }
            isLocked = false;
        }
        if (isLocked && isPlayerNear) coinReducer();
    }

    void coinReducer()
    {
        if(coinNeedToUnlock>0&& GameManager.instance.maxCoin > 0)
        {
            coinNeedToUnlock -= coinSpeed * Time.deltaTime;
            GameManager.instance.maxCoin -= coinSpeed * Time.deltaTime;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("Player") && other.GetComponent<movePlayer>().direction.magnitude < 0.1f)
        {
            isPlayerNear = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear)
        {
            SaveAndLoadData.instance.saveData();
            LevelManager.Instance.saveCoin();
            isPlayerNear = false;
        }
    }
}
