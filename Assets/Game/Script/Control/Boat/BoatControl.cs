using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatControl : MonoBehaviour
{
    public static BoatControl instance;
    public List<Transform>BoatSeattingPosition=new List<Transform>();
    public List<GameObject>aMan=new List<GameObject>();
    public Transform Parant;
    public Transform DriverPosition;

    private void Start() {
        instance =this;
    }
    private void Update() {
        foreach(Transform t in Parant){
            if (!t.GetComponent<BoatSeat>().isOccupied && !BoatSeattingPosition.Contains(t) && t.gameObject.activeSelf) BoatSeattingPosition.Add(t);
            if(t.GetComponent<BoatSeat>().isOccupied && BoatSeattingPosition.Contains(t))BoatSeattingPosition.Remove(t);
        }
    }

    
    // public void boardArmyToBoat(){
    //     if(aMan.Count>0){
    //         for (int i = 0; i < aMan.Count-1; )
    //         {
    //             GameObject a = aMan[i];
    //             a.GetComponent<aMemberMove>().enabled=false;
    //             a.GetComponent<aMemberMoveOnBoat>().endPosition = BoatSeattingPosition[0];
    //             BoatSeattingPosition[0].GetComponent<BoatSeat>().isOccupied=true;
    //             a.GetComponent<aMemberMoveOnBoat>().moveToBoat();
    //             i++;
    //         }
    //         /*foreach(GameObject a in aMan){
    //             a.GetComponent<aMemberMove>().enabled=false;
    //             a.GetComponent<aMemberMoveOnBoat>().endPosition = BoatSeattingPosition[0];
    //             BoatSeattingPosition[0].GetComponent<BoatSeat>().isOccupied=true;
    //             a.GetComponent<aMemberMoveOnBoat>().moveToBoat();
    //         }*/
    //     }
    // }

}
