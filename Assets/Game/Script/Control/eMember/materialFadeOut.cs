using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class materialFadeOut : MonoBehaviour
{
    public bool multipleMaterial = false;
    public bool startFadingOut;
     SkinnedMeshRenderer render;
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<SkinnedMeshRenderer>();
    }

    float x = 1;
    // Update is called once per frame
    void Update()
    {
       
        /*if (!startFadingOut)
        {
            if (multipleMaterial)
            {
                for (int i = 0; i < render.materials.Length; i++)
                {
                    render.materials[i].color = new Color(render.materials[i].color.r, render.materials[i].color.g, render.materials[i].color.b, 1);
                }
                if (x > 0) x -= Time.deltaTime;
            }
            if (!multipleMaterial)
            {
                if (x > 0) x -= Time.deltaTime;
                render.material.color = new Color(render.material.color.r, render.material.color.g, render.material.color.b, 1);
            }
        }*/
    }

    public void fadeIN()
    {
        if (startFadingOut)
        {
            if (multipleMaterial)
            {
                for (int i = 0; i < render.materials.Length; i++)
                {
                    render.materials[i].color = new Color(render.materials[i].color.r, render.materials[i].color.g, render.materials[i].color.b, x);
                }
                if (x > 0) x -= Time.deltaTime;
            }
            if (!multipleMaterial)
            {
                if (x > 0) x -= Time.deltaTime;
                render.material.color = new Color(render.material.color.r, render.material.color.g, render.material.color.b, x);
            }
        }
    }
}
