using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eMemberMatChange : MonoBehaviour
{
    public Material Level_1, Level_2, Level_3;
    public GameObject AandPGameObject_1, AandPGameObject_2, AandPGameObject_3;
    public bool[] Level;

    public SkinnedMeshRenderer[] SMR;

    public bool isMaterialChanged = false;
    private void Start()
    {
        
    }
    void Update()
    {
        if(!isMaterialChanged)
        changeMaterialLevelVice();
    }
    void changeMaterialLevelVice()
    {
        if (Level[0])
        {

            materialAdd(SMR[0], Level_1,1);
            SMR[1].material = Level_1;
            materialAdd(SMR[2], Level_1,0);
            materialAdd(SMR[2], Level_1,1);
            AandPGameObject_1.SetActive(true);
            isMaterialChanged = true;
        }
        if (Level[1])
        {

            materialAdd(SMR[0], Level_2, 1);
            SMR[1].material = Level_2;
            materialAdd(SMR[2], Level_2, 0);
            materialAdd(SMR[2], Level_2, 1);
            AandPGameObject_2.SetActive(true);
            isMaterialChanged = true;
        }
        if (Level[2])
        {

            materialAdd(SMR[0], Level_3, 1);
            SMR[1].material = Level_3;
            materialAdd(SMR[2], Level_3, 0);
            materialAdd(SMR[2], Level_3, 1);
            AandPGameObject_3.SetActive(true);
            isMaterialChanged = true;
        }
    }

    void materialAdd(SkinnedMeshRenderer smr, Material smrmat, int i)
    {
        Material[] mats = smr.materials;
        Material mat = smrmat;
        mats[i] = mat;
        smr.materials = mats;
    }
}
