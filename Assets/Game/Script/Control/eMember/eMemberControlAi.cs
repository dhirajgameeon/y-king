using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class eMemberControlAi : MonoBehaviour
{
    public List<GameObject> Coin = new List<GameObject>();
    private TownHallManager hallManager;
    public EnemyWarrior warrior;
    public GameObject pWarrior;
    public eMemberHealthControl hCon;
    public Animator animator;
    public ParticleSystem deathEffect;
    public ParticleSystem Aura;
    public float deathDealyTime = 0.5f;
    public bool isDead = false;


    //this will happens only if pWarrior is empty
    public bool isUnknownInRange;
    public GameObject unknownWarrior;

    // Start is called before the first frame update
    float x;
    void Start()
    {
        x = 0;
        hallManager = GetComponentInParent<TownHallManager>();
    }

    // Update is called once per frame
    void Update()
    {
        vfxAdd();
        if (pWarrior != null) cP();

        if (warrior.isWarriorInAttackRange && pWarrior)
        {
            Attack();
            EnemyDead();
        }
        if(!pWarrior)warrior.isWarriorInAttackRange = false;
        if (!warrior.isWarriorInAttackRange && !isDead)
        {
            //animator.SetInteger("attack", 0);
            animator.Play("Unarmed Idle");
        }
        Dead();
        if (pWarrior && !pWarrior.activeSelf)
            pWarrior = null;

        if (hCon.health.HP > 0 && pWarrior == null && !hallManager.eMan.Contains(this.gameObject))
        {
            hallManager.eMan.Add(this.gameObject);
        }
    }
    
    void vfxAdd()
    {
   /*     if(!deathEffect && GetComponentInChildren<eManVFXBuff>().GetComponent<ParticleSystem>().gameObject.activeSelf)
            deathEffect = GetComponentInChildren<eManVFXBuff>().GetComponent<ParticleSystem>();

        if(!Aura && GetComponentInChildren<eManVFXAura>().GetComponent<ParticleSystem>().gameObject.activeSelf)
            Aura = GetComponentInChildren<eManVFXAura>().GetComponent<ParticleSystem>();*/
    }
    void cP()
    {
        //check player warrior is in range
        warrior.isWarriorInAttackRange = Vector3.Distance(transform.position, pWarrior.transform.position) <= warrior.AttackRange ? true : false;
        if (pWarrior.GetComponent<movePlayer>() && Vector3.Distance(transform.position, pWarrior.transform.position) > warrior.AttackRange)
        {
            pWarrior = null;
        }
    }
    void Attack()
    {
        if (x > 0)
            x -= Time.deltaTime;
        if (x <= 0 && !animator.GetCurrentAnimatorStateInfo(0).IsName("Standing Melee Attack Downward"))
        {
            //animator.SetInteger("attack", Random.Range(1, 4));
            if(Aura.isPlaying)Aura.Stop();
            AM.am.aS.PlayOneShot(AM.am.WAttack[Random.Range(0, AM.am.WAttack.Length)]);
            animator.SetTrigger("attack");
            transform.LookAt(new Vector3(pWarrior.transform.position.x, transform.position.y, pWarrior.transform.position.z));
            
            x = hCon.health.attackSpeed;
        }
    }
    public void hitEnemy()
    {
        if (warrior.isWarriorInAttackRange && pWarrior && pWarrior.GetComponent<playerHealthControl>().health.HP > 0)
        {
            pWarrior.GetComponent<playerHealthControl>().health.HP -= hCon.health.attackDamage;            
        }
    }
    public void hitBlood()
    {
        if (warrior.isWarriorInAttackRange && pWarrior && pWarrior.GetComponent<playerHealthControl>().health.HP > 0)
        {            
            if(pWarrior.GetComponent<playerHealthControl>().health.blood)
                pWarrior.GetComponent<playerHealthControl>().health.blood.Play();
        }
    }
    void Dead()
    {
        if (hCon.health.HP <= 0 && !isDead)
        {
            isDead = true;
            StartCoroutine(deathDelay(deathDealyTime));
        }
    }
    IEnumerator deathDelay(float t)
    {
        AM.am.aS.PlayOneShot(AM.am.WDeath[Random.Range(0, AM.am.WDeath.Length)]);
        deadAnimation();
        pWarrior = null;
        giveCoin();
        yield return new WaitForSeconds(0.45f);
        deathEffect.Play();
        deathEffect.transform.parent = null;
        yield return new WaitForSeconds(t);
        this.gameObject.SetActive(false);
        isDead = false;
    }
    void giveCoin()
    {
        foreach (GameObject a in Coin)
            a.SetActive(true);
    }
    void deadAnimation()
    {
        int i = Random.Range(0, 2);
        switch (i)
        {
            case 0:
                animator.Play("Falling Back Death");
                animator.Play("FadeOut");
                break;
            case 1:
                animator.Play("Falling Back Death");
                animator.Play("FadeOut");
                break;
        }

    }
    void EnemyDead()
    {
        if (pWarrior.GetComponent<playerHealthControl>().health.HP <= 0.1f && !hallManager.eMan.Contains(this.gameObject))
        {
            if (hCon.health.HP > 0)
            {
                hallManager.eMan.Insert(0,this.gameObject);
                pWarrior = null;
            }
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = warrior.AttackRangeColor;
        Handles.DrawWireDisc(transform.position, warrior.discDirection, warrior.AttackRange, 7);
    }
#endif
}
