using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayerAi : MonoBehaviour
{
    public Animator anime;
    public float movementTime;
    public bool isMoving;
    public bool isReched=false;
    public bool isDeboard=false;
    public bool isBoard=false;
    public movePlayer moveP;
    private playerControl control;
    private void Start() {
        control = GetComponent<playerControl>();
    }
    private void Update() {
        if(isMoving){
            moveP.isMove=false;
            //moveP.Joystick.enabled=false;
        }else{
            moveP.isMove=true;
            //moveP.Joystick.enabled=true;
        }
        move();
    }
    public void move(){
        if(isMoving)
        {
            anime.SetFloat("speed",1);
            transform.LookAt(new Vector3(control.endPosition.position.x,transform.position.y,control.endPosition.position.z));
        }
        if(!isMoving)
            anime.SetFloat("speed",0);

        if(control.endPosition && !isMoving && !isReched)
        {
            if(!isDeboard)moveP.enabled=false;
            LeanTween.move(this.gameObject,control.endPosition,movementTime).setOnComplete(()=>
            {
                moveP.direction = Vector3.zero;
                LeanTween.rotateLocal(this.gameObject,new Vector3(0,0,0),0.3f).setOnComplete(()=>{
                    if(isDeboard)
                    {
                        GetComponent<movePlayerAi>().enabled=false;                        
                        moveP.enabled=true;
                        playerControl.instance.isPlayerOnBoat=false;
                        playerControl.instance.isPlayerLeftBoat=false;
                        isDeboard=false;
                    }
                });               
                if (!isDeboard)
                {
                    transform.parent = control.endPosition;
                    moveP.enabled = false;
                    FindObjectOfType<BoatMove>().enabled = true;
                    playerControl.instance.isPlayerOnBoat = true;
                }
                isMoving = false;
                isReched = true;
            });
                isMoving=true;
        }
        
    }
}
