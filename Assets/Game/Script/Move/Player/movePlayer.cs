using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayer : MonoBehaviour
{
    public Joystick Joystick;
    public Transform groundC;
    public float GroundOffset = 0.15f;
    public float speed;
    public float rotationSmooth;
    public float IsometricRotate;
    public float maintainDistanceFromArmy;

    public LayerMask ground;

    private CharacterController CharacterController;
    private float turnSmoothVelocity;

    public bool isMove = false;
    void Start()
    {
        CharacterController = GetComponent<CharacterController>();        
    }


    void Update()
    {
        MovePlayer();
        GameManager.instance.report1.text = direction.ToString();
    }

    [HideInInspector] public Vector3 direction;
    public void MovePlayer()
    {
        if (isMove)
        {
            float x = -Joystick.Vertical;
            float z = Joystick.Horizontal;
            direction = new Vector3(x, 0, z).normalized;
        }
        if (!isMove)
        {
            float x = 0;
            float z = 0;
            direction = new Vector3(x, 0, z).normalized;
        }

        if (direction.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle + 45, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
            Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * new Vector3(1, 0, 1);
            CharacterController.Move(moveDir.normalized * speed * Time.deltaTime);
        }
       CharacterController.Move(-transform.up * 5 * Time.deltaTime);
    }
}
