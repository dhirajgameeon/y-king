using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMove : MonoBehaviour
{   public Joystick Joystick;
    public float GroundOffset = 0.15f;
    public float speed;
    public float rotationSmooth;

    private CharacterController CharacterController;
    private float turnSmoothVelocity;
    //private Transform cam;

    bool isGrounded;
    float gravity;
    Vector3 velocity;
    void Start()
    {
        CharacterController = GetComponent<CharacterController>();
    }


    void Update()
    {
        MovePlayer();
        transform.position = new Vector3(transform.position.x, GroundOffset, transform.position.z);
    }

    [HideInInspector] public Vector3 direction;
    public void MovePlayer()
    {
        float x = -Joystick.Vertical;
        float z = Joystick.Horizontal;

        direction = new Vector3(x, 0, z).normalized;

        if (direction.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle+45, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * new Vector3(1, 0, 1);
            CharacterController.Move(moveDir.normalized * speed * Time.deltaTime);
        }
    }

}
