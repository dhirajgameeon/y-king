using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class aMemberMove : MonoBehaviour
{
    [HideInInspector]public NavMeshAgent agent;
    public Animator anime;
    public Transform DestinationToMove;
    public GameObject aura;
    public float rotationFace = 90;
    public float stoppingDistance = 3;

    

    public bool followingPlayer;
    public bool isMoving;
    public bool isReched;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        agent.stoppingDistance = stoppingDistance;
        
    }


    void Update()
    {
        moveAgent();
        if(followingPlayer) aura.GetComponent<ParticleSystem>().Stop();
    }

    public void moveAgent()
    {
        anime.SetFloat("speed",agent.velocity.magnitude);
        try
        {
            if (isReched && DestinationToMove && DestinationToMove.GetComponent<waitingArea>())
            {
                rotationFace = DestinationToMove.GetComponent<waitingArea>().rotatingFace;
                LeanTween.rotate(this.gameObject, new Vector3(0, rotationFace, 0), 0.3f).setOnComplete(()=>{
                    isReched = false;
                    aura.SetActive(true);
                });                
            }
        }
        catch
        {
            //Debug.Log("Destination don't have <waitingArea> Script");
        }
               
        if (DestinationToMove)
        {
            agent.SetDestination(DestinationToMove.position);
            if (Vector3.Distance(transform.position, DestinationToMove.position) <= (stoppingDistance+1) && !isMoving)
            {
                isReched = true;
            }            
        }
        if (isMoving)
        {
            isReched = false;
        }
            if (agent.velocity.magnitude > 0.1) isMoving = true; else { isMoving = false; }
    }
}
