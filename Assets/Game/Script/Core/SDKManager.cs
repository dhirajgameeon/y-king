using UnityEngine;
using Facebook.Unity;
using GameAnalyticsSDK;

public class SDKManager : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
        FB.Init();
        GameAnalytics.Initialize();
        HapticFeedback.SetVibrationOn(true);
    }
}
