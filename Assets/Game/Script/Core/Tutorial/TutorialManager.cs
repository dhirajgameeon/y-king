using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TutorialManager : MonoBehaviour
{
    public List<TownHallManager> THM = new List<TownHallManager>();
    public playerControl pControl;
    public BoatControl bControl;
    public Transform FollowButton;
    public GameObject pBase;
    public GameObject pArrow;
    public GameObject bArrow;

    private Transform lookTowards;
    private WarriorManager wm;
    public bool playerOnIsland = false;

    public bool calledOnce = false;
    public bool win = false;
    void Start()
    {
        wm = WarriorManager.instance;   
    }

    void Update()
    {
        if (!calledOnce)
        {
            callOnce();
        }
        if (calledOnce)
        {
            checkForTH();
            playerWithBoat();
            checkForBoat();
            isPOI();
        }
        arrowRotation();
    }

    void arrowRotation()
    {
        if (lookTowards)
        {
            if (bArrow.activeSelf) bArrow.transform.LookAt(new Vector3(lookTowards.position.x, bArrow.transform.position.y, lookTowards.position.z));
            if (pArrow.activeSelf) pArrow.transform.LookAt(new Vector3(lookTowards.position.x, pArrow.transform.position.y, lookTowards.position.z));
        }
    }
    void playerWithBoat()
    {
        if ( pControl.isPlayerOnBoat && pControl.aMan.Count <= 0)
        {
            pArrow.SetActive(false);
            bArrow.SetActive(true);
        }
        if (pControl.aMan.Count <= 0 && !pControl.isPlayerOnBoat && !win) 
        {
            pArrow.SetActive(false);
            bArrow.SetActive(false);
        }
        if (pControl.aMan.Count > 0 && !pControl.isPlayerOnBoat && !playerOnIsland)
        {
            pArrow.SetActive(true);
            bArrow.SetActive(false);
        }
        if (playerOnIsland && !win)
        {
            pArrow.SetActive(false);
        }
        if (!pControl.isPlayerOnBoat) bArrow.SetActive(false);
    }
    void isPOI()
    {
        foreach (var thm in THM)
        {
            if (thm.player)
            {
                playerOnIsland = true;
                break;
            }
            else playerOnIsland = false;
        }
    }
    void checkForBoat()
    {
        if (pControl.aMan.Count > 0 && !pControl.isPlayerOnBoat)
            lookTowards = bControl.transform;
        if (pControl.aMan.Count <= 0 && !pControl.isPlayerOnBoat)
            lookTowards = pBase.transform;
    }
    void checkForTH()
    {
        foreach (var TH in THM.ToArray())
        {
            if (TH.landConquer)
            {
                win = true;
                pArrow.SetActive(true);
                lookTowards = bControl.transform;
                if(pArrow.activeSelf)
                    THM.Remove(TH);
            }
        }

        if (pControl.isPlayerOnBoat && !THM[0].landConquer && bControl.aMan.Count > 0)
            lookTowards = THM[0].transform;

/*        if (pControl.isPlayerOnBoat && !THM[0].landConquer && bControl.aMan.Count < 0)
            lookTowards = pBase.transform;*/
    }
    void callOnce()
    {
        bool towardsFollowButton = wm.ActiveArmyMembers.TrueForAll(member => member.GetComponent<aMemberMove>().isReched == true);
        if (towardsFollowButton)
        {
            pArrow.SetActive(true);
            lookTowards = FollowButton;
        }
    }
}
