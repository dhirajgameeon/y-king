using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class afkWManager : MonoBehaviour
{
    public List<Transform> afkPosition = new List<Transform>();
    public List<GameObject> afkWarrior = new List<GameObject>();
    public List<GameObject> activeAfkWarrior = new List<GameObject>();

    [Space(5)]
    public Transform afkList;
    public int activeListCount;
    private void Start()
    {
    }

    private void Update()
    {
    }
   public void saveData()
    {
        foreach(Transform t in afkList)
        {
            if(t.gameObject.activeSelf && !activeAfkWarrior.Contains(t.gameObject))
            {
                activeAfkWarrior.Add(t.gameObject);
            }
        }
        activeListCount = activeAfkWarrior.Count;
    }
    public void loadWarrior()
    {
        for (int i = 0; i < activeListCount; i++)
        {
            afkWarrior[0].GetComponent<moveAFKW>().isOccupied = true;
            afkWarrior.Remove(afkWarrior[0]);
            afkWarrior[0].SetActive(true);
        }
    }
}
