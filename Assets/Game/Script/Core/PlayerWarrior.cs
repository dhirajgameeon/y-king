using UnityEngine;

[System.Serializable]
public class PlayerWarrior 
{
    public bool isEnemyInAttackRange;
    public float AttackRange = 10;
    public Color AttackRangeColor;
    public Vector3 discDirection = new Vector3(0, 1, 0);
}
