using UnityEngine;

[System.Serializable]
public class HealthAttribute 
{
    public float HP = 100f;
    public float MaxHealth = 100f;
    public float attackSpeed = 1.2f;
    public float attackDamage = 10;    
    public ParticleSystem blood;
}
