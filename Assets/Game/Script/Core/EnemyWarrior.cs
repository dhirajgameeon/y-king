using UnityEngine;

[System.Serializable]
public class EnemyWarrior 
{
    public bool isWarriorInAttackRange;
    public float AttackRange = 10;
    public Color AttackRangeColor;
    public Vector3 discDirection = new Vector3(0, 1, 0);
    public LayerMask Player;
}
