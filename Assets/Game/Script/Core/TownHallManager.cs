using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TownHallManager : MonoBehaviour
{
    public static TownHallManager instance;
    public afkWManager awm;
    public List<GameObject> eMan = new List<GameObject>();
    public List<GameObject> cEnemyUnitAlive = new List<GameObject>();
    public EnemyBase eBase;
    //public List<GameObject> feMan = new List<GameObject>(); //for front row of enemy
    //public List<GameObject> beMan = new List<GameObject>(); //for back row of enemy

    public GameObject UIWinPanal;
    public GameObject UI;
    public GameObject player;

    [Header("Castle")]
    [Space(5)]
    public GameObject REC;
    public GameObject BLC;

    [Space(5)]
    public bool attackButtonPressed;
    public bool attackInProgress;
    public bool landConquer;

    public float distanceForRETREAT = 10;
    private void Awake()
    {
        instance = this;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        awm = GetComponent<afkWManager>();
        if (landConquer)Conquered();
        foreach(GameObject e in eMan)
        {
            if (!cEnemyUnitAlive.Contains(e))
                cEnemyUnitAlive.Add(e);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!landConquer) {
            checkPlayer();
            Attack();
            UIManager();
            retretePlayer();
        }
        
        for (int i = 0; i <= eMan.Count - 1; i++)
        {
            if (!eMan[i].activeSelf)
                eMan.Remove(eMan[i]);
        }
        for (int i = 0; i <= cEnemyUnitAlive.Count - 1; i++)
        {
            if (!cEnemyUnitAlive[i].activeSelf)
                cEnemyUnitAlive.Remove(cEnemyUnitAlive[i]);
        }       
    }
    void UIManager()
    {
        if(eBase.isPlayerInAttackRange && !attackInProgress && player.GetComponent<playerControl>().aMan.Count > 0 && !landConquer) UI.SetActive(true);
        if(!eBase.isPlayerInAttackRange || attackButtonPressed && attackInProgress) UI.SetActive(false);

        if (player && player.GetComponent<playerControl>().aMan.Count <= 0 && player.GetComponent<playerControl>().aManWithPlayer.Count <= 0)
        {
            attackInProgress = false;
            WarriorManager.instance.isArmyOnWar = false;
        }
            
            //if (eMan.Count <= 0 && playerControl.instance.aMan.Count > 0) attackInProgress = false;

            if (cEnemyUnitAlive.Count <= 0 && !landConquer) {
            attackInProgress = false;
            attackButtonPressed = true;
            landConquer = true;
            UI.SetActive(false);
            awm.saveData();
            SaveAndLoadData.instance.saveData();
            StartCoroutine(openWInPanal());
        }
    }

    void Conquered()
    {
        foreach (var c in eMan)
        {
            if (c.activeSelf)
                c.SetActive(false);
        }
        REC.SetActive(false);
        BLC.SetActive(true);
        awm.loadWarrior();
    }
    public IEnumerator openWInPanal()
    {
        yield return new WaitForSeconds(1f);
        if (!UIWinPanal.activeSelf)
        {
            AM.am.aS.PlayOneShot(AM.am.BattleVictory);
            UIWinPanal.SetActive(true);
        }
        yield return new WaitForSeconds(1.5f);
        REC.SetActive(false);
        BLC.SetActive(true);
        /*UIWinPanal.GetComponent<Animator>().Play("WinPanalOut");*/
    }
    public void AttackButton()
    {
        attackButtonPressed = true;
        attackInProgress = true;
    }
    float x = 0.1f;
    int i = 0;
    public void Attack()
    {
        if (attackButtonPressed)
        {
            if(x>0)
                x -= Time.deltaTime;
            if (x <= 0 && eMan.Count>0)
            {
                /*GameObject pW = player.GetComponent<playerControl>().aMan[player.GetComponent<playerControl>().aMan.Count - 1];
                GameObject eW = eMan[eMan.Count - 1];*/
                GameObject pW = player.GetComponent<playerControl>().aMan[0];
                GameObject eW = eMan[0];

                pW.GetComponent<aMemberMove>().DestinationToMove = eW.transform;
                pW.GetComponent<aMemberMove>().agent.stoppingDistance = 2.5f;
                pW.GetComponent<aMemberMove>().stoppingDistance = 2.5f;
                pW.GetComponent<aMemberControl>().hallManager = GetComponent<TownHallManager>();

                eW.GetComponent<eMemberControlAi>().pWarrior = pW;
                //
                eMan.Remove(eW);
                player.GetComponent<playerControl>().aMan.Remove(pW);
                x = 0.1f;
            }

            if (x <= 0 && eMan.Count <= 0 && player.GetComponent<playerControl>().aMan.Count>0)
            {
                
                /*GameObject pW = player.GetComponent<playerControl>().aMan[player.GetComponent<playerControl>().aMan.Count - 1];
                GameObject eW = eMan[eMan.Count - 1];*/
                GameObject pW = player.GetComponent<playerControl>().aMan[0];
                GameObject eW = cEnemyUnitAlive[0];

                pW.GetComponent<aMemberMove>().DestinationToMove = eW.transform;
                pW.GetComponent<aMemberMove>().agent.stoppingDistance = 2.5f;
                pW.GetComponent<aMemberMove>().stoppingDistance = 2.5f;
                pW.GetComponent<aMemberControl>().hallManager = GetComponent<TownHallManager>();

                eW.GetComponent<eMemberControlAi>().pWarrior = pW;
                //
                /*eMan.Remove(eW);*/
                player.GetComponent<playerControl>().aMan.Remove(pW);
/*                if (i < cEnemyUnitAlive.Count)
                    i++;*/
                x = 0.1f;
                print("<color=red>Took From Here</color>");
            }
/*            if (i > cEnemyUnitAlive.Count - 1)
                i = 0;*/

            if (player.GetComponent<playerControl>().aMan.Count <= 0)
                attackButtonPressed = false;
        }

        if(player && player.GetComponent<playerControl>().aMan.Count>0 && attackInProgress)
        {
            attackButtonPressed = true;
        }
        
    }
    void checkPlayer()
    {
        eBase.isPlayerOnLand = Physics.CheckSphere(transform.position, eBase.LandRange,eBase.Player);
        eBase.isPlayerInAttackRange = Physics.CheckSphere(transform.position, eBase.AttackRange,eBase.Player);
        if(eBase.isPlayerOnLand) player = GameObject.Find("Player");
        if (!eBase.isPlayerOnLand) player = null;
    }
    void retretePlayer()
    {
        if (player && Vector3.Distance(player.transform.position, player.GetComponent<playerControl>().endPosition.position) > distanceForRETREAT)
        {
            if ( cEnemyUnitAlive.Count > 0 && player.GetComponent<playerControl>().aManWithPlayer.Count <= 0)
            {
                player.GetComponent<movePlayerAi>().anime.ResetTrigger("attack");
                player.GetComponent<movePlayerAi>().enabled = true;
                player.GetComponent<movePlayerAi>().moveP.enabled = false;
                player.GetComponent<movePlayerAi>().isReched = false;
                player.GetComponent<movePlayerAi>().isDeboard = true;
                GameManager.instance.RETREAT.SetActive(true);
            }
            //GameManager.instance.report.text = Vector3.Distance(player.transform.position, player.GetComponent<playerControl>().endPosition.position).ToString();
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = eBase.LandRangeColor;
        Handles.DrawWireDisc(transform.position, eBase.discDirection, eBase.LandRange, 8);
        Handles.color = eBase.AttackRangeColor;
        Handles.DrawWireDisc(transform.position, eBase.discDirection, eBase.AttackRange, 8);
    }
#endif
}
