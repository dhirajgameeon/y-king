using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerBase : MonoBehaviour
{
    public float HealingRange;
    public Vector3 Offset;
    public Color healingColor;

    private Transform pc;
    void Start()
    {
        pc = playerControl.instance.transform;
    }

    // Update is called once per frame
    void Update()
    {
        heal();
    }

    void heal()
    {
        if (Vector3.Distance(transform.position, pc.position) <= HealingRange)
        {
            pc.GetComponent<playerHealthControl>().health.HP = pc.GetComponent<playerHealthControl>().health.MaxHealth;
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = healingColor;
        Handles.DrawWireDisc(transform.position+ Offset, new Vector3(0,1,0), HealingRange, 4);
    }
#endif
}
