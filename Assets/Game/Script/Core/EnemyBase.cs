using UnityEngine;
[System.Serializable]
public class EnemyBase 
{
    [Header("Enemy Base Identifire")]
    public bool isPlayerInAttackRange;
    public bool isPlayerOnLand;
    public float AttackRange = 10;
    public Color AttackRangeColor;
    public float LandRange = 15;
    public Color LandRangeColor;
    public Vector3 discDirection;
    public LayerMask Player;
}
