using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AM : MonoBehaviour
{
    public static AM am;

    public AudioClip CC;
    public AudioClip UnlockOrUpgrade;
    public AudioClip BattleVictory;
    public AudioClip LevelComplete;
    public AudioClip WSpwan;
    public AudioClip[] WAttack;
    public AudioClip[] WDeath;


    public AudioSource aS;

    private void Awake()
    {
        am = this;
    }
    private void Start()
    {
        aS = GetComponent<AudioSource>();
    }
}