using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{


    public static GameManager instance;
    public TextMeshProUGUI report;
    public TextMeshProUGUI report1;
    [Header("Coin Manager")][Space(5)]
    public float maxCoin;
    public TextMeshProUGUI coinText;
    public float currentCoin;
    public float coinSpeed;



    [Header("Game Over")]
    [Space(5)]
    public GameObject WinPartical;
    public bool GameOver = false;
    public bool GameComplete = false;



    [Header("UI Manager")]
    [Space(5)]
    public List<TownHallManager> THM = new List<TownHallManager>();
    public TextMeshProUGUI TownHallCountText;
    public TextMeshProUGUI playerWarriorCountText;
    
    [Header("Progress Slider")]
    public Slider progressBar;
    public float maxThm;
    public float sliderSpeed;

    [Header("RETREAT")]
    public GameObject RETREAT;

    [Header("Haptic Feedback Manager")]
    [Space(5)]
    public int vibrationLenght = 200;
    void Awake()
    {
        instance = this;   

    }
    private void Start()
    {
        Application.targetFrameRate = 60;
        progressBar.maxValue = 4;
        if (PlayerPrefs.HasKey("coin"))
        {
            maxCoin = PlayerPrefs.GetFloat("coin");
        }

    }

    void Update()
    {
        coinText.text = maxCoin.ToString("N0");
        
        checkTHMCount();
        thmCounter();
        GC();
    }

    void coinManagement()
    {
        if (maxCoin <= 0)
        {
            maxCoin = 0;
        }
    }

    void checkTHMCount()
    {
        TownHallCountText.text = (Mathf.Abs(THM.Count - 5)).ToString("N0") + "/5";



        playerWarriorCountText.text = playerControl.instance.aManWithPlayer.Count.ToString("N0");
        foreach (TownHallManager t in THM.ToArray())
        {
            if (t.landConquer)
                THM.Remove(t);
        }
    }
    void thmCounter()
    {        
        if(Mathf.Abs(4 - THM.Count) > maxThm)
        {
            maxThm += sliderSpeed * Time.deltaTime;
            if (maxThm >= Mathf.Abs(4 - THM.Count))
                maxThm = Mathf.Abs(4 - THM.Count);
        }
        if (Mathf.Abs(4 - THM.Count) < maxThm)
        {
            maxThm -= sliderSpeed * Time.deltaTime;
            if (maxThm <= Mathf.Abs(4 - THM.Count))
                maxThm = Mathf.Abs(4 - THM.Count);
        }
        progressBar.value = maxThm;
    }
    void GC()
    {
        if (THM.Count <= 0 && !GameComplete)
        {
            AM.am.aS.PlayOneShot(AM.am.BattleVictory);
            WinPartical.SetActive(true);
            GameComplete = true;            
        }
    }
}
