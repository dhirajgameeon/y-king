[System.Serializable]
public class GameData
{
    public bool TH1;
    public int TH1AFKCount;
    public bool TH2;
    public int TH2AFKCount;
    public bool TH3;
    public int TH3AFKCount;
    public bool TH4;
    public int TH4AFKCount;

    public int WaitingArea1 = 15;
    public int WaitingArea2 = 30;
    public int WaitingArea3 = 60;
    public GameData()
    {
        TH1 = false;
        TH1AFKCount = 0;
        TH2 = false;
        TH2AFKCount = 0;
        TH3 = false;
        TH3AFKCount = 0;    
        TH4 = false;
        TH4AFKCount = 0;

        WaitingArea1 = 15;
        WaitingArea2 = 30;
        WaitingArea3 = 60;
    }

}
