using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveAndLoadData : MonoBehaviour
{
    public static SaveAndLoadData instance;

    public List<TownHallManager> thm = new List<TownHallManager>();
    public List<waitingAreaUnlockPoint> waitingAreas = new List<waitingAreaUnlockPoint>();

    public GameData data;
    public string path;
    private void Awake()
    {
        instance = this;
        data = SaveManager.Load(path);
        loadData();
    }
    public void loadData()
    {
        thm[0].landConquer = data.TH1;
        thm[0].awm.activeListCount = data.TH1AFKCount;

        thm[1].landConquer = data.TH2;
        thm[1].awm.activeListCount = data.TH2AFKCount;

        thm[2].landConquer = data.TH3;
        thm[2].awm.activeListCount = data.TH3AFKCount;

        thm[3].landConquer = data.TH4;
        thm[3].awm.activeListCount = data.TH4AFKCount;

        waitingAreas[0].coinNeedToUnlock = data.WaitingArea1;
        waitingAreas[1].coinNeedToUnlock = data.WaitingArea2;
        waitingAreas[2].coinNeedToUnlock = data.WaitingArea3;

        print("DATA LOADED");
    }
    public void saveData()
    {
        data.TH1 = thm[0].landConquer;
        data.TH1AFKCount = thm[0].awm.activeListCount;
        data.TH2 = thm[1].landConquer;
        data.TH2AFKCount = thm[1].awm.activeListCount;
        data.TH3 = thm[2].landConquer;
        data.TH3AFKCount = thm[2].awm.activeListCount;
        data.TH4 = thm[3].landConquer;
        data.TH4AFKCount = thm[3].awm.activeListCount;

        data.WaitingArea1 = (int)waitingAreas[0].coinNeedToUnlock;
        data.WaitingArea2 = (int)waitingAreas[1].coinNeedToUnlock;
        data.WaitingArea3 = (int)waitingAreas[2].coinNeedToUnlock;


        SaveManager.Save(data, path);
        print("DATA SAVED");
    }

    public void ResetData()
    {
        data.TH1 = false;
        data.TH1AFKCount = 0;
        data.TH2 = false;
        data.TH2AFKCount = 0;
        data.TH3 = false;
        data.TH3AFKCount = 0;
        data.TH4 = false;
        data.TH4AFKCount = 0;

        data.WaitingArea1 = 15;
        data.WaitingArea1 = 30;
        data.WaitingArea1 = 60;
        SaveManager.Save(data, path);
        print("DATA RESETED");
    }
}
