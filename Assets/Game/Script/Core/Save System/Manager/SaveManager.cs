using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveManager
{
    public static void Save(GameData data, string fileName)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream fs = new FileStream(GetPath(fileName), FileMode.Create);
        formatter.Serialize(fs, data);
        fs.Close();
    }
    public static GameData Load(string fileName)
    {
        if (!File.Exists(GetPath(fileName)))
        {
            GameData emptyData = new GameData();
            Save(emptyData, fileName);
            return emptyData;
        }

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream fs = new FileStream(GetPath(fileName), FileMode.Open);
        GameData data = formatter.Deserialize(fs) as GameData;
        fs.Close();
        return data;
    }
    private static string GetPath(string fileName)
    {
        return Application.persistentDataPath + fileName;
    }
}
