using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    public GameObject WinPanal;
    public TextMeshProUGUI levelText;
    private GameManager gm;

    private void Awake()
    {
        Instance = this;    
        //DontDestroyOnLoad(this);
    }

    public int LevelCount;
    private const string LevelCountKey = "Level";
    private void Start()
    {       
        gm = GameManager.instance;
        LoadScene();
        levelText.text = "Level : " + (LevelCount + 1).ToString();
    }
    void LoadScene()
    {
        if (PlayerPrefs.HasKey(LevelCountKey)) LevelCount = PlayerPrefs.GetInt(LevelCountKey);
        if (PlayerPrefs.HasKey("coin")) gm.maxCoin = PlayerPrefs.GetFloat("coin");
        Scene currentScene = SceneManager.GetActiveScene();
        if (currentScene.buildIndex != LevelCount % 3)
            SceneManager.LoadScene(LevelCount % 3);
    }

    private void OnApplicationQuit()
    {
        saveCoin();
    }
    private void Update()
    {
        loadWinPanal();
        reset();
    }    
    public void saveCoin()
    {
        PlayerPrefs.SetFloat("coin", gm.maxCoin);
        PlayerPrefs.Save();
    }
    
    void reset()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlayerPrefs.SetFloat("coin", 0);
            PlayerPrefs.SetInt(LevelCountKey, 0);
            PlayerPrefs.Save();
        }
    }
    public void loadWinPanal()
    {
        if (gm.GameComplete)
        {
            StartCoroutine(LoadPanalDeal(2.5f));
        }
    }
    public void NextLevel()
    {
        SaveAndLoadData.instance.ResetData();
        LevelCount += 1;
        PlayerPrefs.SetInt(LevelCountKey,LevelCount);
        PlayerPrefs.SetFloat("coin", gm.maxCoin);
        PlayerPrefs.Save();
        SceneManager.LoadScene(LevelCount % 3);
    }
    IEnumerator LoadPanalDeal(float t)
    {
        yield return new WaitForSeconds(t);
        WinPanal.SetActive(true);
    }
}
